"""
PGN Scraper is a small program which downloads each of a user's archived
games at chess.com and stores them locally.
"""
from datetime import datetime
import json
import logging
import logging.config
import urllib.request
import config as cfg


def beginScrape():
    """
    The downloading of the PGN archives happens here.
    The file is saved as "username_YYYYMMddThhmmss.pgn"
    :return:
    """

    now = datetime.now()
    dt_string = now.strftime("%Y%m%dT%H%M%S")
    file_name = f"{cfg.chessDotCom['user']}_{dt_string}.pgn"

    with urllib.request.urlopen(f"{cfg.chessDotCom['host']}/pub/player/"
                                f"{cfg.chessDotCom['user']}/games/archives") \
            as url:
        archives = list(dict(json.loads(url.read().decode()))["archives"])

        for archive in archives:
            with urllib.request.urlopen(archive) as url:
                games = list(dict(json.loads(url.read().decode()))["games"])
                for game in games:
                    logging.info(game["pgn"])
                    with open(file_name, "a") as text_file:
                        print(game["pgn"], file=text_file)


def main():
    """
    Scrape PGN files from chess.com .
    """
    logging.config.fileConfig('logconfig.ini')
    logging.info(f"pgnscraper starting up. "
                 f"Configured to use {cfg.chessDotCom['host']} as "
                 f"{cfg.chessDotCom['user']}")
    beginScrape()

if __name__ == '__main__':
    main()
